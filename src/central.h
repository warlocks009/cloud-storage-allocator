#ifndef __CENTRAL_H__
#define __CENTRAL_H__

#include <stdio.h>
#include <string.h>


int addentry (char*, char*,char*);

int deleteentry (char*);

int addserver (char*,char*,long long int);

int deleteserver (char *);

int modifyspace (char *,long long int);

int changeip (char *,char *);
int fetchip (char *,char*);

long long int fetchsize (char *);

int searchserver (long long int ,char*);

int concatestrings (char*,char*,char*,int);

int searchentry (char *,char*,char*);

#endif

