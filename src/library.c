#include <library.h>

int addid (char *a,char *b, long long int size)
{
	//adds id in the file auth.txt
	FILE *fp;
	fp=fopen("auth.txt","a");
	fprintf(fp,"%s %s %lld\n",a,b,size);
	//printf("%s %s 10000\n",a,b);
	fclose(fp);
	return(0);
}
int deleteid (char *username,char *path,long long  int number)
{
	//deletes the id from the auth.txt file
	FILE *fp,*fp1;
	long long int size;
	int temp=0,i;
	char a[256],b[256];
	fp=fopen("auth.txt","r");
	fp1=fopen("temp.txt","w");		
	while( (i=fscanf(fp,"%s %s %lld",a,b,&size))>0)
	{
		if(strcmp(username,a)||strcmp(path,b)||number!=size)
		{
			fprintf(fp1,"%30s %30s %lld\n",a,b,size);
		}
		else
		{
			temp=1;		
		}	
	}
	fclose(fp1);
	fclose(fp);
	system("mv temp.txt auth.txt");
	return(temp);
}

int match (char *username,char *password)
{
	FILE *fp;
	char a[256],b[256];
	int i;
	long long int len;
	len=strlen(a);
	fp=fopen("auth.txt","r");
	if(fp==0)
	{
		printf("File opening error\n");
		return(0);
	}
	while( (i=fscanf(fp,"%s %s,%lld",a,b,&len))>0)
	{
		if(!strcmp(username,a))
		{
			if(!strcmp(password,b))
			{
				return(0);	
			}
			return(1);
		}
	}
	return(2);
}

int separatestrings (char *a,char *b, char *c)
{
	int i,j;
	i=j=0;
	while(a[i]!=' ')
	{
		b[j++]=a[i++];
	}
	b[j]='\0';
	i++;
	j=0;
	while(a[i]!='\0')
	{
		c[j++]=a[i++];
	}
	c[j]='\0';
	return(0);
}

int separatestringsthree (char *a,char *b, char *c,char *d)
{
	int i,j;
	i=j=0;
	while(a[i]!=' ')
	{
		b[j++]=a[i++];
	}
	b[j]='\0';
	i++;
	j=0;
	while(a[i]!=' ')
	{
		c[j++]=a[i++];
	}
	c[j]='\0';
	i++;
	j=0;
	while(a[i]!='\0')
	{
		d[j++]=a[i++];
	}
	d[j]='\0';
	return(0);
}


int SeparateStringAndNumber (char *a, long long int *c)
{
	long long int temp=0;
	int i;
	i=0;
	while(a[i++]!=' ');

	while(a[i]!='\0')
	{
		if(a[i]>='0'&&a[i]<='9')
		{
			temp=temp*10+a[i]-'0';
		}
		i++;
	}
	*c=temp;
	return(0);
}


int ConvertLongtoString(long long int t,char* a)
{

	int i=0;
	while(t)
	{
		a[i++]=(t%10)+'0';
		t/=10;
	}
	a[i]='\0';
	a=stringreverse(a);
	return(0);
}


char* stringreverse(char *a)
{
	int len,i=0;
	len=strlen(a);
	char c;
	for(i=0;i<len/2;i++)
	{
		c=a[i];
		a[i]=a[len-1-i];
		a[len-1-i]=c;
	}
	return(0);
}


int addslash(char *a)
{
	char temp[256];
	int i=0,len;
	len=strlen(a);
	temp[0]='/';
	for(i=0;i<=len;i++)
	{
		temp[i+1]=a[i];
	}
	strcpy(a,temp);
	return(0);
}

long long  int ConvertStringtoLong(char* a)
{
	long long int temp;
	int i;
	i=0;
	temp=0;
	while(a[i]!='\0')
	{
		temp=temp*10+a[i]-'0';
		i++;		
	}
	return(temp);
}
