/* tcpserver.c */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <library.h>
#include <central.h>
int main()
{

	//variable making this program as a server
	long long int size;
	int i,len;
	char username[256],password[256],servername[256],ip[256];
        int sock, connected, bytes_recieved , true = 1;  
        char send_data [256] , recv_data[256],temp[256],temp2[256],cc[20];       
	char path[256];
        struct sockaddr_in server_addr,client_addr;    
        int sin_size;
        
        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
            perror("Socket");
            exit(1);
        }

        if (setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(int)) == -1) {
            perror("Setsockopt");
            exit(1);
        }
        
	printf("Enter portnumber\n");
	//scanf("%d",&portnumber);
        server_addr.sin_family = AF_INET;         
        server_addr.sin_port = htons(8150);     
        server_addr.sin_addr.s_addr = INADDR_ANY; 
        bzero(&(server_addr.sin_zero),8); 

        if (bind(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr))
                                                                       == -1) {
            perror("Unable to bind");
            exit(1);
        }

        if (listen(sock, 5) == -1) {
            perror("Listen");
            exit(1);
        }
		
	printf("\nTCPServer Waiting for client on port 5000");
        fflush(stdout);


        while(1)
        {  


          sin_size = sizeof(struct sockaddr_in);

            connected = accept(sock, (struct sockaddr *)&client_addr,&sin_size);

            printf("\n I got a connection from (%s , %d)",
                   inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));

            while (1)
            {
       /*       printf("\n SEND (q or Q to quit) : ");
              gets(send_data);
              
              if (strcmp(send_data , "q") == 0 || strcmp(send_data , "Q") == 0)
              {
                send(connected, send_data,strlen(send_data), 0); 
                close(connected);
                break;
              }
               
              else
                 send(connected, send_data,strlen(send_data), 0);  
*/
              bytes_recieved = recv(connected,recv_data,1024,0);

              recv_data[bytes_recieved] = '\0';
//		printf("\n RECIEVED DATA = %s " , recv_data);
              if (strcmp(recv_data , "q") == 0 || strcmp(recv_data , "Q") == 0)
              {
                close(connected);
                break;
              }

              else 
		{
		separatestrings(recv_data,username,password);
		printf("username %s password %s\n",username,password);
		if(!(i=match(username,password)))
		{
			printf("%d",i);
			strcpy(send_data,"ok");			
              		send(connected, send_data,strlen(send_data), 0);  
			sleep(2);			
			printf("Send Data %s\n",send_data);			
			searchentry(username,path,servername);
			fetchip(servername,ip);
			printf("%s\n",ip);
			concatestrings(send_data,ip,path,1);			
			printf("send_data %s\n",send_data);
			goto here;
		}
		else
		{
			printf("%d",i);
			if(i==1)
			{
				strcpy(send_data,"incorrect");
			}
			if(i==2)
			{
				strcpy(send_data,"signup");	
			}
		}		
		}
              fflush(stdout);
              send(connected, send_data,strlen(send_data), 0);  
              bytes_recieved = recv(connected,recv_data,1024,0);

              recv_data[bytes_recieved] = '\0';
              if (strcmp(recv_data , "q") == 0 || strcmp(recv_data , "Q") == 0)
              {
                close(connected);
                break;
              }

              else 
		{
		if(strncmp(recv_data,"signup",6)==0)
		{
			SeparateStringAndNumber(recv_data,&size);
			printf("Size %lld\n",size);
			
			
			if(searchserver(size,servername)==0)
			{
				strcpy(send_data,"notok");
        	    	 	 send(connected, send_data,strlen(send_data), 0);  			
  				close(connected);
				break;										
			}
		printf("Servername %s\n",servername);
		
		strcpy(send_data,"ok");
        	    	 send(connected, send_data,strlen(send_data), 0);  			

			addid(username,password,size);			
		//search for server having at least SIZE available			
		//add entry to server and get path 
			printf("here");

//			strcpy(ip,"127.0.0.1");
			fetchip(servername,ip);		
			printf("IP %s\n",ip);
			strcpy(temp,username);
			addslash(temp);
			addentry(username,temp,servername);			
		
			printf("IP %s\n",ip);
			printf("PATH %s\n",temp);
				
		//make directory of size THIS and name in username in servername
		//	makespace		


	//code calling another distribured server from here
		
		concatestrings(temp2,"./tcpserver2 ",ip,1);
		concatestrings(temp2,temp2,temp,1);
		ConvertLongtoString(size,cc);		
		concatestrings(temp2,temp2,cc,1);	
		concatestrings(temp2,temp2,inet_ntoa(client_addr.sin_addr),1);
		printf("String passed to tcpserver2 %s\n",temp2);
		system(temp2);

	//DISTRIBUTED SERVER CODE WHICH IS HAVING SOME ERROR;
	
	// system call to be called
		
	

			printf("ip %s\n",ip);
			printf("temp %s\n",temp);
			concatestrings(send_data,ip,temp,1);			
		// this ip and send_data/path will be added to 
	
			printf("Send_data %s\n",send_data);
//			strcpy(send_data,"ok");			
		}
		else
		{
		separatestrings(recv_data,username,password);
		if(!(i=match(username,password)))
		{
			strcpy(send_data,"ok");			
             	// 	 send(connected, send_data,strlen(send_data), 0);  			
              		send(connected, send_data,strlen(send_data), 0);  
			sleep(2);			

			searchentry(username,path,temp);
			fetchip(temp,ip);
			addslash(username);			
			concatestrings(send_data,ip,username,1);						
		

		}
		else
		{
			strcpy(send_data,"q");
              	 	 send(connected, send_data,strlen(send_data), 0);  			
			close(connected);
			break;		
		}		
		}
		}
	here:
			printf("send_data %s",send_data);
              	 	 send(connected, send_data,strlen(send_data), 0);  			
			close(connected);
			break;  
      }
       
}
      close(sock);
      return 0;
} 
