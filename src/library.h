#ifndef __LIBRARY_H__
#define __LIBRARY_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>


int addid (char*, char*,long long int);

int deleteid (char*, char*,long long int);

int match (char*, char*);

int separatestrings (char*, char*, char*);

int separatestringsthree (char*, char*, char*,char*);

int SeparateStringAndNumber (char *,long long int *);

int ConvertLongtoString(long long int,char*);

long long  int ConvertStringtoLong(char*);

char* stringreverse(char *);

int addslash(char*);

#endif
