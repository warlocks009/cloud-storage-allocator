mkdir -p $2$3
#dd if=/dev/zero of=/usr/disk-img/disk-quota3.ext3 count=$1
dd if=/dev/zero of=/usr/disk-img$3.ext3 count=$1

#echo $1
/sbin/mkfs -t ext3 -q /usr/disk-img/$3.ext3 -F
echo -n "/usr/disk-img"$3".ext3      "     >>/etc/fstab
echo -n $2$3    >>/etc/fstab
echo "   ext3    rw,loop,usrquota,grpquota  0 0" >> /etc/fstab

echo -n $2$3 >>/etc/exports
echo "       "$4"(rw,nohide,insecure,no_subtree_check,sync)" >>/etc/exports
mount $2$3
/etc/init.d/nfs-kernel-server restart
